import random
import time
import matplotlib.pyplot as plt

TIMES = 1000
DOORS = 3

def main():
    # plt.xlim(0, TIMES)
    time.sleep(1)
    plt.ylim(0, 1)

    fixed_correct_time = 0
    changed_correct_time = 0
    fixed_correct_times = []
    changed_correct_times = []
    for i in range(TIMES):
        win_index = random.randrange(DOORS)
        answer_index = random.randrange(DOORS)
        empty_indexes = [index for index in range(DOORS) if index is not win_index and index is not answer_index]
        open_index = empty_indexes[random.randrange(len(empty_indexes))]

        if answer_index == win_index:
            fixed_correct_time += 1
        if DOORS - (answer_index + open_index):
            changed_correct_time += 1

        fixed_correct_times.append(fixed_correct_time / (i + 1))
        changed_correct_times.append(changed_correct_time / (i + 1))

        f_line, = plt.plot(fixed_correct_times, label='fixed corrects', color='coral')
        c_line, = plt.plot(changed_correct_times, label='changed corrects', color='darkslategray')
        plt.pause(0.0005)
        plt.legend()
        f_line.remove()
        c_line.remove()

        print(f'fixed: {round(fixed_correct_time / (i + 1), 3)}, changed: {round(changed_correct_time / (i + 1), 3)}\r', end='')

    print(f'fixed: {round(fixed_correct_time / (i + 1), 3)}, changed: {round(changed_correct_time / (i + 1), 3)}\r', end='')

    plt.plot(fixed_correct_times, label='fixed corrects', color='coral')
    plt.plot(changed_correct_times, label='changed corrects', color='darkslategray')
    plt.legend()
    plt.show()

if __name__ == "__main__":
    main()